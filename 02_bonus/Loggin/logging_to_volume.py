from http.server import BaseHTTPRequestHandler, HTTPServer


class LoggerClass:
    def __init__(self, source):
        self.logger = self.logger_init(source)

    @staticmethod
    def logger_init(source):
        logger_file = "myvol/{}_{}.log".format(source, datetime.datetime.now().strftime("%Y-%m-%d"))
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        file_handler = logging.FileHandler(logger_file)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        return logger

    def to_logger(self, **kwargs):
        for name, value in kwargs.items():
            if name == "error":
                self.logger.error(value)
            if name == "info":
                self.logger.info(value)
 
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
	def __init__(self):
		self.logger = LoggerClass() 

	def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        message = "testHTTPServer"
        self.wfile.write(bytes(message, "utf8"))
        return
 
def run():
	addr = "127.0.0.1"
	port = 8081
	server_address = (addr, port)
	httpd = HTTPServer(server_address, testHTTPServer_RequestHandler, LoggerClass)
	httpd.logger(info=["starting server... on port {}:{}".formst(addr, port)])
	httpd.logger(info=["server is running..."])
	httpd.serve_forever()
 
run()